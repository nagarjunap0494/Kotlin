package com.navigationdrawer.kotlinone.Fragments

import android.os.Bundle
import android.support.annotation.Nullable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.navigationdrawer.kotlinone.R

class ThirdFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        var view = inflater!!.inflate(R.layout.fragment_third, container, false)
        setUpLayout(view)
        return view
    }

    override fun onResume() {
        super.onResume()

    }

    fun setUpLayout(view: View) {

    }
}
