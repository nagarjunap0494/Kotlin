package com.navigationdrawer.kotlinone.Fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


open class BaseFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.i("Class:", "" + (this as Any).javaClass.simpleName)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        return super.onCreateView(inflater!!, container, savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context?) {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onAttach(context)
    }

    override fun onInflate(context: Context?, attrs: AttributeSet?, savedInstanceState: Bundle?) {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onInflate(context, attrs, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onActivityCreated(savedInstanceState)
    }

    override fun onStart() {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onStart()
    }

    override fun onResume() {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onResume()
    }

    override fun onPause() {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onPause()
    }

    override fun onStop() {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onStop()
    }

    override fun onDestroyView() {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onDestroyView()
    }

    override fun onDestroy() {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onDestroy()
    }

    override fun onDetach() {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onDetach()
    }

    override fun onLowMemory() {
        Log.v("Class:", "" + (this as Any).javaClass.simpleName)
        super.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }
}