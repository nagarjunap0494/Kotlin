package com.navigationdrawer.kotlinone.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.navigationdrawer.kotlinone.Fragments.MainFragment

open class BaseActivity : AppCompatActivity() {

    val TAG: String = javaClass.simpleName
    fun addFragment(fragment: MainFragment, fragment_containerId: Int, addToBackStack: Boolean) {
        supportFragmentManager.beginTransaction().replace(fragment_containerId, fragment, null).commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        Log.i(TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "onStop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.i(TAG, "onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "onDestroy")
    }
}
