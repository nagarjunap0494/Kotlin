package com.navigationdrawer.kotlinone.activity

import android.os.Bundle
import com.navigationdrawer.kotlinone.R

class FirstActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)

    }
}