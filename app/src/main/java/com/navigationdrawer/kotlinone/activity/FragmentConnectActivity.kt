package com.navigationdrawer.kotlinone.activity

import android.os.Bundle
import com.navigationdrawer.kotlinone.Fragments.MainFragment
import com.navigationdrawer.kotlinone.R

class FragmentConnectActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_connect_activity)
        setUpLayout()
    }

    private fun setUpLayout() {
        val fragment = MainFragment()
        val bundle = Bundle()
        fragment.arguments = bundle
        addFragment(fragment, R.id.fragment_container, false)
    }
}