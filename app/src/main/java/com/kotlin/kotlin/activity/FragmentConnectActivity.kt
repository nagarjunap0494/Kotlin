package com.kotlin.kotlin.activity

import android.os.Bundle
import com.kotlin.kotlin.Fragments.MainFragment
import com.kotlin.kotlin.R

class FragmentConnectActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_connect_activity)
        setUpLayout()
    }

    private fun setUpLayout() {
        val fragment = MainFragment()
        val bundle = Bundle()
        fragment.arguments = bundle
        addFragment(fragment, R.id.fragment_container, false)
    }
}