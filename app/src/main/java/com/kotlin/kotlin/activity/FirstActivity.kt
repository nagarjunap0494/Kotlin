package com.kotlin.kotlin.activity

import android.os.Bundle
import com.kotlin.kotlin.R

class FirstActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)

    }
}