package com.navigationdrawer.kotlinone.Fragments

import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.navigationdrawer.kotlinone.R


class MainFragment : BaseFragment() {
    protected var mFragmantManager: FragmentManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFragmantManager = activity!!.supportFragmentManager
    }

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        var view: View = inflater!!.inflate(R.layout.fragment_hello, container, false)
        setUpLayout(view)
        return view
    }

    override fun onResume() {
        super.onResume()

    }

    fun setUpLayout(view: View) {
        var firstFragment: Button = view.findViewById(R.id.first_fragment) as Button
        var secondFragment: Button = view.findViewById(R.id.second_fragment) as Button
        var thirdFragment: Button = view.findViewById(R.id.third_fragment) as Button
        var forthFragment: Button = view.findViewById(R.id.forth_fragment) as Button
        var fifthFragment: Button = view.findViewById(R.id.fifth_fragment) as Button


        firstFragment.setOnClickListener {

            fragmentManager!!.beginTransaction().replace(R.id.fragment_container, FirstFragment(), null)
                    .addToBackStack(null)
                    .commit()
        }
        secondFragment.setOnClickListener {

            fragmentManager!!.beginTransaction().replace(R.id.fragment_container, SecondFragment(), null)
                    .addToBackStack(null)
                    .commit()
        }
        thirdFragment.setOnClickListener {
            fragmentManager!!.beginTransaction().replace(R.id.fragment_container, ThirdFragment(), null)
                    .addToBackStack(null)
                    .commit()
        }
        forthFragment.setOnClickListener {
            fragmentManager!!.beginTransaction().replace(R.id.fragment_container, ForthFragment(), null)
                    .addToBackStack(null)
                    .commit()
        }
        fifthFragment.setOnClickListener {
            fragmentManager!!.beginTransaction().replace(R.id.fragment_container, FifthFragment(), null)
                    .addToBackStack(null)
                    .commit()
        }
    }
}
