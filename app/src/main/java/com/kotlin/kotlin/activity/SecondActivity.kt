package com.kotlin.kotlin.activity

import android.os.Bundle
import com.kotlin.kotlin.R
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        /*  Kotlin Android Extensions is basically a view binder that will let you use your XML views
        in your code by just using their id. It will automatically create properties for them without
        using any external annotation or findViewById methods.*/

        editName.setText("Name is Naga")
        editPassword.setText("password")

        textView.text = "simple text for testing"
        passwordText.text = "polam"

    }
}