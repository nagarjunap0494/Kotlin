package com.navigationdrawer.kotlinone.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.navigationdrawer.kotlinone.R

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupLayout()
    }

    fun setupLayout() {
        //var --> variables defined with var are mutable(Read and Write)
        //val --> variables defined with val are immutable(Read only)

        /* Fetching Id's */
        var textView: TextView = findViewById(R.id.textview) as TextView
        var buttonOne: Button = findViewById(R.id.button_one) as Button
        var buttonTwo: Button = findViewById(R.id.button_two) as Button
        var buttonBaseActivity: Button = findViewById(R.id.button_base) as Button

        /* Setting values to textviews and buttons */
        textView.text = "Click ME TextView"
        buttonOne.text = "First Activity"
        buttonTwo.text = "Second Activity"

        /*Click listener and navigating to different screens*/
        buttonOne.setOnClickListener {
            var activityIntent = Intent(this, FirstActivity::class.java)
            startActivity(activityIntent)
        }

        buttonTwo.setOnClickListener {
            var activityThree = Intent(this, SecondActivity::class.java)
            startActivity(activityThree)
        }

        buttonBaseActivity.setOnClickListener {
            var baseActivity = Intent(this, FragmentConnectActivity::class.java)
            startActivity(baseActivity)
        }

        /**/
        var z: View = findViewById(R.id.my_view)
        if (z is TextView) {
            z.text = "I am Textview "
        }
    }
}