package com.navigationdrawer.kotlinone.Fragments

import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.navigationdrawer.kotlinone.R

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ForthFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ForthFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ForthFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        var view = inflater!!.inflate(R.layout.fragment_forth, container, false)
        setUpLayout(view)
        return view
    }

    override fun onResume() {
        super.onResume()

    }

    fun setUpLayout(view: View) {

    }
}
